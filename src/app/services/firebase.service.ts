import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Message } from '../models/usuario.interface';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor( public afDB: AngularFireDatabase, private authService: AuthService,private db: AngularFirestore) { 
  }
  //Traer lista de usuarios
  getTodos(){
    return this.afDB.list('usuarios/');
  }
  //Busqueda de usuario por nombre del usuario
  public getUsuarios(nUser){  
    if(nUser == null){
      //Trae todos los usuarios
      return this.afDB.list('usuarios/', ref =>{
        return ref.limitToFirst(20);
      });
    }else{
      //Trae al usuario con en nombre del usuario
      return this.afDB.list('usuarios/', ref => {
        return ref.orderByChild('nuser').equalTo(nUser);
      });
    }
  }
  //Busqueda de usuario por id
  getUsuario(idUsuario){
    return this.afDB.object('usuarios/'+idUsuario);
  }
  //Actualizar usuario por id
  updateUsuario(usuario){
    return  this.afDB.database.ref('usuarios/'+this.authService.getUser()).set(usuario);
  }
  //Agregar usuario
  addUsuario(usuario){
    return this.afDB.database.ref('usuarios/'+this.authService.getUser()).set(usuario);
  }
  //Eliminar usuario por id
  removeUsuario(){
    return this.afDB.database.ref('usuarios/'+this.authService.getUser()).remove();
  }
  /* *****************************************************************************
                          CRUD COLLECTION IMAGENES
  *****************************************************************************/
  //Obtener referencias de imagenes ordenadas por id
  getCollections(idUsuario){
    //Referencia a la colecciones de imagenes en la base de datos cloud firebase ordenandolo de forma decendente
    return this.afDB.list('imagenes/'+idUsuario+'/', ref => {
      return ref.orderByChild('id');
    });
  }
  //Actualizar usuario por id
  updateCollection(imagen){
    return  this.afDB.database.ref('imagenes/'+this.authService.getUser()+'/'+(-imagen.id)).set(imagen);
  }
  //Agregar usuario
  addCollection(imagen){
    return this.afDB.database.ref('imagenes/'+this.authService.getUser()+'/'+(-imagen.id)).set(imagen);
  }
  //Eliminar usuario por id
  removeCollection(idimagen){
    return this.afDB.database.ref('imagenes/'+this.authService.getUser()+'/'+(-idimagen)).remove();
  }

  /* *****************************************************************************
                          CRUD COLLECTION ID MENSAJES
  *****************************************************************************/
  //Obtener referencias de imagenes ordenadas por id
  getCollectionsIdUsuarios(){
    //Referencia a la colecciones de imagenes en la base de datos cloud firebase ordenandolo de forma decendente
    return this.afDB.list('listaMensajes/'+this.authService.getUser(), ref => {
      return ref.orderByChild('fecha');
    });
  }
  getCollectionsMensajes(usuarios){
    //Referencia a la colecciones de imagenes en la base de datos cloud firebase ordenandolo de forma decendente
    return this.afDB.list('usuarios/', ref => {
      return ref.orderByChild('id').equalTo(usuarios);
    });
  }
  //Actualizar usuario por id
  updateCollectionMensajes(imagen){
    return  this.afDB.database.ref('listaMensajes/'+this.authService.getUser()+'/'+(-imagen.id)).set(imagen);
  }
  //Agregar usuario
  addCollectionMensajes(usuario,usuarior,user2){
    this.afDB.database.ref('listaMensajes/'+this.authService.getUser()+'/'+usuario.id).set(usuarior);
    this.afDB.database.ref('listaMensajes/'+usuario.id+'/'+this.authService.getUser()).set(user2);
  }
  //Eliminar usuario por id
  removeCollectionMensajes(idimagen){
    return this.afDB.database.ref('listaMensajes/'+this.authService.getUser()+'/'+(-idimagen)).remove();
  }

  /* *****************************************************************************
                          CRUD COLLECTION MENSAJES
  *****************************************************************************/
  
  getChats(id){
    return this.db.collection('chatsRoom').doc(id).valueChanges();
  }
  createChat(id,message) {
    return this.db.collection('chatsRoom').doc(id).set(message);
  }
  sendMsgToFirebase(message: Message, id){
    this.db.collection('chatsRoom').doc(id).update({
      messages: firestore.FieldValue.arrayUnion(message),
    })
  }

  /* *****************************************************************************
                          CRUD COLLECTION NOTICIAS
  *****************************************************************************/
 //Busqueda de noticia por hora guaarada
  getTodosMuro(hora){
  return this.afDB.list('muro/', ref => {
    return ref.orderByChild('hora').startAt(-hora).limitToFirst(15);
   
  });
}

//Busqueda de noticia por id
getMuro(id){
  return this.afDB.object('muro/'+id);
}
//Actualizar noticia por id
updateMuro(id,usuario){
  return  this.afDB.database.ref('muro/'+id).set(usuario);
}
//Agregar noticia
addMuro(usuario){
  return this.afDB.database.ref('muro/'+usuario.hora).set(usuario);
}
addLike(id,id2,likes){
  return this.afDB.database.ref('muro/'+id+'/'+'likes/'+id2).set(likes);
}
//Eliminar noticia por id
removeMuro(id){
  return this.afDB.database.ref('muro/'+id).remove();
}

 /* *****************************************************************************
                          CRUD COLLECTION BADGES
  *****************************************************************************/
 //Busqueda de noticia por hora guaarada
 getBadges(){
  return this.afDB.list('badges/'+this.authService.getUser());  
  }

  //Busqueda de usuario por id
  getBadge(id){
    return this.afDB.object('badges/'+this.authService.getUser()+'/'+id);
  }
  
  //Actualizar noticia por id
updateBadges(usuario){
  return  this.afDB.database.ref('badges/'+this.authService.getUser()).set(usuario);
}
//Agregar noticia
addBadges(usuario,mensaje){
  return this.afDB.database.ref('badges/'+usuario.id+'/'+this.authService.getUser()).set(mensaje);
}
//Eliminar noticia por id
removeBadges(usuario){
  console.log(usuario)
  return this.afDB.database.ref('badges/'+this.authService.getUser()+'/'+usuario).remove();
}
removeMensaje(usuario){
  return this.afDB.database.ref('listaMensajes/'+this.authService.getUser()+'/'+usuario.id).set(usuario);
}

/* *****************************************************************************
                          CRUD Eventos
  *****************************************************************************/
 //Busqueda de noticia por hora guaarada
 getEventos(){
  return this.afDB.list('eventos/');  
  }

  //Busqueda de usuario por id
  getEvento(){
    return this.afDB.object('eventos/'+this.authService.getUser());
  }
  
  //Actualizar noticia por id
updateEvento(usuario){
  return  this.afDB.database.ref('eventos/'+this.authService.getUser()).set(usuario);
}
//Agregar noticia
addEvento(usuario,mensaje){
  return this.afDB.database.ref('eventos/'+this.authService.getUser()).set(mensaje);
}
//Eliminar noticia por id
removeEvento(usuario){
  console.log(usuario)
  return this.afDB.database.ref('eventos/'+this.authService.getUser()).remove();
}


/* *****************************************************************************
                          CRUD CASAS
  *****************************************************************************/
 //Busqueda de noticia por hora guaarada
 getCasas(tipo){
  return this.afDB.list('casas/', ref => {
    return ref.orderByChild('tipo').equalTo(tipo);
  });

  }


  //Busqueda de usuario por id
  getCasa(){
    return this.afDB.object('casas/'+this.authService.getUser());
  }
  
  //Actualizar noticia por id
updateCasa(usuario){
  return  this.afDB.database.ref('casas/'+this.authService.getUser()).set(usuario);
}
//Agregar noticia
addECasa(usuario,mensaje){
  return this.afDB.database.ref('casas/'+this.authService.getUser()).set(mensaje);
}
//Eliminar noticia por id
removeCasa(usuario){
  console.log(usuario)
  return this.afDB.database.ref('casas/'+this.authService.getUser()).remove();
}

 
}
