import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
import { Message } from '../models/usuario.interface';
import { firestore } from 'firebase';

 
@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  constructor(private db: AngularFirestore) { }

  getChatBase(){
    return this.db.collection('chatsRoom').snapshotChanges();
  }

  getChatRooms(id){
    return this.db.collection('chatsRoom').doc(id).valueChanges();
  }
  createCat(id,message) {
    return this.db.collection('chatsRoom').doc(id).set(message);
  }
  sendMsgToFirebase(message: Message, id){
    this.db.collection('chatsRoom').doc(id).update({
      messages: firestore.FieldValue.arrayUnion(message),
    })
  }
}
