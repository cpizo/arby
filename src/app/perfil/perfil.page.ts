import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  // Variable para guardar el id traido de otra pagina
  id=null;
  // Variable que guarda datos obtenido del usuario
  usuario: any = {id: null, nombre: null, nuser: null, ciudad: null, url: null};
  // Variable para guardar imagenes
  imagens:any [];

  constructor(public activateRoute:ActivatedRoute,private firebaseService: FirebaseService) { 
    //Obtener id desde otra pagina
    this.id=this.activateRoute.snapshot.paramMap.get('id');        
  }

  ngOnInit() {
    //Trae los datos del usuario buscandolo con su id
    this.firebaseService.getUsuario(this.id)
      .valueChanges().subscribe(usuario => {
        //Carga la variable usuario con los datos traidos de la base de datos
        this.usuario = usuario;
        //Obtener Lista de colleciones de imagnes
        this.firebaseService.getCollections(this.usuario.id).valueChanges().subscribe( imagens => {
          this.imagens = imagens;
        });     
    });   
  }
  // metodo para mostrar modal
  mostrarModal(){
    let imagenes = document.querySelectorAll('.grid-gallery__image');
    let modal = document.querySelector('#codal');
    let img = document.querySelector('#codal__img');

    for(let i=0; i < imagenes.length; i++){
      imagenes[i].addEventListener('click', function(){
        modal.classList.toggle("codal--open");
        img.setAttribute("src",this.src);
      });
    }   
  }
  //Metodo para cerrar modal
  cerrarModal(){
    document.querySelector('#codal').classList.toggle("codal--open");
  }
}
