import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
    // Variable para guardar la lista chats cargados de la base de datos
    usuarios:any [];
  constructor(public fireBaseDB: FirebaseService) { }

  ngOnInit() {
    // Trae la lista desde firebase 
    this.fireBaseDB.getCollectionsIdUsuarios().valueChanges().subscribe( usuarios => {
      this.usuarios = usuarios;
    });
  }

}
