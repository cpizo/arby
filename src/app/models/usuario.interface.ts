//Interfaz para obtener y guardar usuarios en la base de datos
export interface Usuario {
    id?:string;
    nombre: string;
    correo: string;
    telefono: string;
    ciudad: string;
    password: string;
    nuser:string;
    url:string;
    mensaje:number;
}
//Interfaz para obtener y guardar imagenes en el almacenamiento de firebase
export interface MyData {
    name: string;
    filepath: string;
    size: number;
    id:number;
  }

//Interfaz para obtener y guardar usuario de autenticacios en la base de datos
export class User{
    email: string;
    password: string;
}
//Interfaz para obtener y guardar usuario de autenticacios en la base de datos
export class ListaMensaje{
    id?:string;
    numero: number;
}
export class Message{
    content: string;
    type: string;
    date: Date;
}
export class Badges{
    id?: string;
    mensajes: string;
}