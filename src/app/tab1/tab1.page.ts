import { Component, OnInit } from '@angular/core';
import { MyData } from '../models/usuario.interface';
import { Observable } from 'rxjs';
import { FirebaseService } from '../services/firebase.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize, tap } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  
  usuario: any = {};
  imagens:any [];
 // Cargar Tarea
  task: AngularFireUploadTask; 
  // Progreso en porcentaje
  percentage: Observable<number>; 
  // Instantánea de subir archivo
  snapshot: Observable<any>; 
  // URL del archivo cargado
  UploadedFileURL: Observable<string>; 
  //Detalles de archivos  
  fileSize:number; 
  //Variables para esconder elementos en el html
  isUploading:boolean;
  isUploaded:boolean;

  badges:any;

  constructor(private authService: AuthService, private firebaseService: FirebaseService, private loadingController: LoadingController, private storage: AngularFireStorage, public alertCtrl: AlertController, private router: Router) {
    /* Variables para comenzar con el elemento mostrando 
    foto de perfil actual y boton de cambiar foto */            
    this.isUploading = false;
    this.isUploaded = true;

    
  }

  ngOnInit() {
    //Suscripción a la sesión para saber si esta iniciada
    this.authService.Session.subscribe(async session => {
      //Si session existe entrar al algoritmo
      if (session) {
        //Alert cargando mientras se traen los datos
        const loading = await this.loadingController.create({
          message: 'Cargando....'
         });
        await loading.present();
        //Trae los datos del usuario buscandolo con su id
        this.firebaseService.getUsuario(session.uid)
        .valueChanges().subscribe(usuario => {
          //Carga la variable usuario con los datos traidos de la base de datos
          this.usuario = usuario;
          if(this.usuario.dark){
            document.body.classList.add('dark');
          }else{
            console.log('modo oscuro desactivado');
          }
          //Referencia a la colecciones de imagenes en la base de datos cloud firebase ordenandolo de forma decendente
          this.firebaseService.getCollections(this.usuario.id).valueChanges().subscribe( imagens => {
            this.imagens = imagens;

          this.firebaseService.getBadges().valueChanges().subscribe(badges=>{
            this.badges = badges.length;
            console.log(this.badges);
          })  
          });
        });           
        //Metodo para salir del alert cargando
        loading.dismiss();
      }else{
      console.log('Session = null');
      this.router.navigateByUrl('/login');
      }
    });
  }


uploadFile(event: FileList) { 
  // El objeto del archivo
  const file = event.item(0);
  // Validación para lo que se suba sea una imagen
  if (file.type.split('/')[0] !== 'image') {
   console.error('tipo de archivo no es soportado :( ')
    return;
  }
  //Poder ver el elemento de carga en el html
  this.isUploading = true;
  this.isUploaded = false;
 
  /* Ubicación de donde se va a guardar la imagen, van a ser guardada 
  la carpeta con el nombre del id del usuario y el nombre la hora actual */
  const path = `${this.authService.getUser()}/${new Date().getTime()}`;
 
  // Metadato opcional de la imagen
  const customMetadata = { app: 'Imagen subida desde arby app' };
 
  //Variable de referencia de las imagenes subidas
  const fileRef = this.storage.ref(path);
 
  // Subir imagen al almacenamiento de firebase
  this.task = this.storage.upload(path, file, { customMetadata });
 
  // Obtener progreso en porcentaje 
  this.percentage = this.task.percentageChanges();

  this.snapshot = this.task.snapshotChanges().pipe(      
      finalize(() => {
        // Obtener la ruta de almacenamiento del archivo cargado
        this.UploadedFileURL = fileRef.getDownloadURL();
        //Suscribirse para obtener los elementos de la imagen guardada en el almacenamiento
        this.UploadedFileURL.subscribe(resp=>{
          this.addImagetoDB({name: file.name, filepath: resp, size: this.fileSize, id:Date.now()*-1});
          //Ocultar sesión de carga y mostrar el modulo de foto y botin de cambio de foto
          this.isUploading = false;
          this.isUploaded = true;
        },error=>{
          console.error(error);
        })
      }),
      tap(snap => {
        //Obtener tamaño del archivo
          this.fileSize = snap.totalBytes;
      })
    )
  }
 
  addImagetoDB(image: MyData) {
    //Metodo para agregar referencia a la base de datos en la imagenes
    this.firebaseService.addCollection(image);
    this.usuario.foto = image.filepath;
    this.usuario.hora = image.id;
    this.usuario.likes = 0;
    this.firebaseService.addMuro(this.usuario);
    
  }

   //Metodo para eliminar referencia de fotos de firebase 
   eliminarFoto(idimagen){
    this.firebaseService.removeCollection(idimagen);
    this.firebaseService.removeMuro(idimagen)
  }

  //Metodo para abrir el modal
  mostrarModal(){
    let imagenes = document.querySelectorAll('.grid-gallery__image');
    let modal = document.querySelector('#modal');
    let img = document.querySelector('#modal__img');

    for(let i=0; i < imagenes.length; i++){
      imagenes[i].addEventListener('click', function(){
        modal.classList.toggle("modal--open");
        img.setAttribute("src",this.src);
      });
    }   
  }
  //Metodo para cerrar modal
  cerrarModal(){
    document.querySelector('#modal').classList.toggle("modal--open");
  }

  //Mensaje de alerta
  async showConfirm(idimagen) {
    const prompt = await this.alertCtrl.create({
      header: 'Eliminar esta foto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Eliminar',
          cssClass: 'danger',
          handler: () => {
            //Metodo para eliminar referencia de la foto firebase
            this.eliminarFoto(idimagen);
          }
        }
      ]
    });
    await prompt.present();
  }
    
 
}