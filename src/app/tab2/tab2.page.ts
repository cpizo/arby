import { Component, ViewChild } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  // variable para guardar las primeras publicaciones
  publicaciones:any =[];
  publicacionlike:any;
  likes:any =[] ;
  reaciones:any;
  // variable para  traer las demas publicaciones y aumentarlas al primer arreglo de publicaciones
  otras_publicaciones:any =[];
  // Variable utilizada para obtener la hora actual y traer las publicaciones segun la hora actual
  hora:any;
  // Utilizado para el scroll infiinito
  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
  constructor( public fireBaseDB: FirebaseService, private authService: AuthService) {    
      this.publicacion();
  }
  // Metodo para mostrar las primeras publicaciones
  publicacion(){
    this.hora=Date.now(); 
    this.fireBaseDB.getTodosMuro(this.hora).valueChanges().subscribe( publicaciones => {
      // Guarda las publicacones de la base de datos en el arrar publicaciones
      this.publicaciones = publicaciones; 
      console.log(Object.values(this.publicaciones[0].likes).length)
      // console.log(publicaciones)     
      });
  }
  // Metodo para traer las demas publicaciones
  agregarPublicaciones(){ 
    this.fireBaseDB.getTodosMuro(-(1+this.publicaciones[this.publicaciones.length-1].hora)).valueChanges().subscribe( publicaciones => {
      // Guarda las publicacones de la base de datos en el array otras_publicaciones
      this.otras_publicaciones=publicaciones;
      // Combina las dos arrays
      Array.prototype.push.apply(this.publicaciones,this.otras_publicaciones)
      
      });
  }
  // Metodo para recargar publicaciones desde la ultima vista
  public recargaPublicaciones(){
    this.fireBaseDB.getTodosMuro(-(1+this.publicaciones[this.publicaciones.length-1].hora)).valueChanges().subscribe( publicaciones => {
      // Guarda las publicacones de la base de datos en el array publicaciones
      this.publicaciones=publicaciones;
      });
  }
  // Metodo para regargar los array de imagenes
  loadData(event) {    
    setTimeout(() => {   
      // Metodo para al llegar a 60 imagenes deje de cargar el array   
      if(this.publicaciones.length>60){
         // Metodo para acabar con la carga del scroll infinito
        event.target.complete();
         // Metodo para recargar publicaciones desde la ultima vista
        this.recargaPublicaciones();
        return;
      }
      event.target.complete();
      // Metodo para traer las demas publicaciones
      this.agregarPublicaciones();     
    // Tiempo de carga para el scroll
    },2000);
  }
  // Metodo para refrescar las publicaciones
  doRefresh(event) {
    setTimeout(() => {
      // Metodo para mostrar las primeras publicaciones
      this.publicacion();
      event.target.complete();
      // Tiempo de carga para la refrescar noticias
    }, 2000);
  }  

  like(id){
      this.fireBaseDB.getUsuario(this.authService.getUser()).valueChanges().subscribe( publicacion =>{
      this.publicacionlike = publicacion;
      this.likes.id = this.authService.getUser();
      this.likes.nombre = this.publicacionlike.nombre;
      this.likes.foto = this.publicacionlike.url;
      this.fireBaseDB.addLike(id,this.authService.getUser(),this.likes)
      
    })
    
  }
}