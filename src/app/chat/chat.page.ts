import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { Message, ListaMensaje} from '../models/usuario.interface';
import { AuthService } from '../services/auth.service';
import { IonContent } from '@ionic/angular';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})

export class ChatPage implements OnInit {
  //Variable que agrega usuario a la lista de usuarios
  usuario:ListaMensaje ={numero:0, id:''};
  //trae los datos del usuario del chat
  user:any= {id:null, nombre:null, nuser:null, ciudad:null, url:null, fecha:null,mensaje:null};
  //trae los datos de la  sesion
  user2:any= {id:null, nombre:null, nuser:null, ciudad:null, url:null, fecha:null, mensaje:null};
  // Contiene el id obtenido desde el routing
  id:any;
  idUser:any;
  // Variable para el input de texto del chat
  msg:string;
  // Guardad el chat optenido
  chat:any;
  //Variable para tomar decisiones
  paso:any;

  mensaje:any = {id:null,mensajes:null};
  
  mensaje2:any = {mensajes:null};
  //Decoración utilizada para enviar el scrol a la parte de abajo
  @ViewChild(IonContent, {static: false} )
  content: IonContent

  constructor(public activateRoute:ActivatedRoute,private firebaseService: FirebaseService, private authService: AuthService) { 
    // Obtiene el id desde otra pagina
    this.id=this.activateRoute.snapshot.paramMap.get('id');
    this.idUser= this.authService.getUser();
    // Carga la variable user con  los datos del usuario del chat
    this.firebaseService.getUsuario(this.id)
        .valueChanges().subscribe(usuario => {
          //Carga la variable usuario con los datos traidos de la base de datos
          this.user = usuario;
          this.user.mensaje=0;
          this.limpiarBadges();  
          this.firebaseService.removeMensaje(this.user);
                 
          });
    // Carga la variable user con  los datos del usuario de la  sesion      
    this.firebaseService.getUsuario(this.authService.getUser())
          .valueChanges().subscribe(usuario => {
            //Carga la variable usuario con los datos traidos de la base de datos
            this.user2 = usuario;
            }); 
            
          
          
            
  }

  ngOnInit() { 
     this.firebaseService.getBadge(this.id).valueChanges().subscribe( bage => {
      if(bage){
      this.mensaje =bage;
      }else{
        console.log('no hay badges')
      }

          // Carga la variable user con  los datos del usuario del chat
    this.firebaseService.getUsuario(this.id)
    .valueChanges().subscribe(usuario => {
      //Carga la variable usuario con los datos traidos de la base de datos
      this.user = usuario;
      this.user.mensaje=0;
      this.limpiarBadges();  
      this.firebaseService.removeMensaje(this.user);
             
      });
    });  


    //Trae los chats dede la base de datos dependiendo como se hallan guardado
    this.firebaseService.getChats(this.idUser+this.id).subscribe( chat =>  {
          // Envia el scroll a la parte de abajo despues de 3 segundos
    setTimeout(()=>{
      this.content.scrollToBottom();
    },(3000))    
      if(chat){
        this.chat = chat;
        this.paso=1;
      }
      // Si la primera combinacion no existe
      else{
        this.firebaseService.getChats(this.id+this.idUser).subscribe( chat =>  {
              // Envia el scroll a la parte de abajo despues de 3 segundos
    setTimeout(()=>{
      this.content.scrollToBottom();
    },(3000))    
            if(chat){
              this.chat = chat;
              this.paso=2;
            }
            // Si no existe el chat
            else{
              this.paso=3;
            }
        })      
      }
    })
    // Envia el scroll a la parte de abajo despues de 3 segundos
    setTimeout(()=>{
      this.content.scrollToBottom();
    },(3000));      
  }
  // Metodo para enviar mensaje a la base de datos
  enviar(){
    //Metodo para agregar usuario a la lista de mendajes  
    this.addImagetoDB();
    //Cuerpo del chat
    this.addBadges()
    const mensaje: Message = {
      content : this.msg,
      type: this.user2.id,
      date: new Date()
    }
    //Desición para saber a que dirección enviar el mensaje
    switch(this.paso){
      case 1: this.firebaseService.sendMsgToFirebase(mensaje,this.user2.id+this.id);
              this.msg=""; 
      break;
      case 2: this.firebaseService.sendMsgToFirebase(mensaje,this.id+this.user2.id);
              this.msg=""; 
      break;
      //Opción que crea el chat si no existe
      case 3: this.firebaseService.createChat(this.user2.id+this.id,mensaje);
              this.firebaseService.sendMsgToFirebase(mensaje,this.user2.id+this.id);              
              this.msg="";       
      break;
    }
    // Bajar scroll cuando se envie un mensaje
    setTimeout(()=>{
       this.content.scrollToBottom();
     },(300))    
  }
  //Metodo para agregar usuario a la lista de mendajes
  addImagetoDB() {
    
    //Guardar en usuario.id la id obtenida de otra pagina
    this.usuario.id=this.id;
     this.user.fecha = -Date.now();
     this.user2.fecha = -Date.now();
    this.user2.mensaje = this.mensaje.mensajes+1;
    
    //Metodo para agregar referencia a la base de datos de lista de mensajes
    this.firebaseService.addCollectionMensajes(this.usuario,this.user,this.user2); 
  }
  addBadges(){
    this.mensaje.id = this.id;
    this.mensaje.mensajes=this.mensaje.mensajes+1;
    this.firebaseService.addBadges(this.user,this.mensaje);
  }
  limpiarBadges(){
    this.firebaseService.removeBadges(this.id);
    
  }
  
}
