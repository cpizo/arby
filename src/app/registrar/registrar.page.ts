import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User, MyData } from '../models/usuario.interface';
import { FirebaseService } from '../services/firebase.service';
import { Usuario } from '../models/usuario.interface';
import { LoadingController, NavController, AlertController, IonSlides } from '@ionic/angular';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {
  //variable que contiene la interface del modelo del usuarion
  usuario:Usuario ={
    nombre:'',
    correo:'',
    telefono:'',
    ciudad:'',
    password:'',
    nuser:'',
    url:'',
    mensaje:0
  };
  //Variable para asignar alerta
  caso:number;
  /*****************************************************************
   Variable utilizada en alguno de los dos campos de password para
   verificar si los dos campos de contraseña som iguales
  ******************************************************************/
  passwordConfirm:string;

  /*****************************************************************
   Variable comtiene el user.correo y user.password del usuario
  ******************************************************************/
  user: User = new User();
  /*****************************************************************
   Variable boolean para el toggle y abilitar y desabilitar el boton
   registrar
  ******************************************************************/
  activo:boolean = true;

  url: any = {url: null};
   // Cargar Tarea
   task: AngularFireUploadTask;
   // Progreso en porcentaje
   percentage: Observable<number>;
   // Instantánea de subir archivo
   snapshot: Observable<any>;
   // URL del archivo cargado
   UploadedFileURL: Observable<string>;
   // Lista imagen subida
   images: Observable<MyData[]>;
   //Detalles de archivo  
   fileSize:number;
   //Variables para esconder elementos en el html
   isUploading:boolean;
   isUploaded:boolean;
 

  @ViewChild(IonSlides,{static: false}) slides: IonSlides;


  constructor(private todoService: FirebaseService, private loadingController: LoadingController, private nav: NavController, private authService: AuthService, public alertCtrl: AlertController, private router: Router,  private firebaseService: FirebaseService, private storage: AngularFireStorage) { 
        /* Variables para comenzar con el elemento mostrando 
    foto de perfil actual y boton de cambiar foto */
    this.isUploading = false;
    this.isUploaded = true;
    this.usuario.url = 'https://firebasestorage.googleapis.com/v0/b/arby-229d3.appspot.com/o/1467646262_522853_1467646344_noticia_normal.jpg?alt=media&token=519bc17e-aa07-4013-b9e0-eac718dcd166';
  }

  ngOnInit() {
   
  }
  uploadFile(event: FileList) { 
    // El objeto del archivo
    const file = event.item(0)
   
    // Validación para lo que se suba sea una imagen
    if (file.type.split('/')[0] !== 'image') {
     console.error('tipo de archivo no es soportado :( ')
      return;
    }
    //Poder ver el elemento de carga en el html
    this.isUploading = true;
    this.isUploaded = false;
   
  /*   Ubicación de donde se va a guardar la imagen, van a ser guardada 
    la carpeta con el nombre del id del usuario y el nombre la hora actual */
    const path = `${'primera foto'}/${new Date().getTime()}`;
   
    // Metadato opcional de la imagen
    const customMetadata = { app: 'Imagen subida desde arby app' };
   
    //Variable de referencia de las imagenes subidas
    const fileRef = this.storage.ref(path);
   
    // Subir imagen al almacenamiento de firebase
    this.task = this.storage.upload(path, file, { customMetadata });
   
    // Obtener progreso en porcentaje 
    this.percentage = this.task.percentageChanges();
  
    this.snapshot = this.task.snapshotChanges().pipe(      
        finalize(() => {
          // Obtener la ruta de almacenamiento del archivo cargado
          this.UploadedFileURL = fileRef.getDownloadURL();
          //Suscribirse para obtener los elementos de la imagen guardada en el almacenamiento
          this.UploadedFileURL.subscribe(resp=>{
             this. darurl({name: file.name, filepath: resp, size: this.fileSize, id:Date.now()});
            //Ocultar sesión de carga y mostrar el modulo de foto y botin de cambio de foto
            this.isUploading = false;
            this.isUploaded = true;
          },error=>{
            console.error(error);
          })
        }),
        tap(snap => {
          //Obtener tamaño del archivo
            this.fileSize = snap.totalBytes;
        })
      )
    }
  goToSlide() {
    this.slides.slideTo(1, 500);
  }
  goToSlide2() {
    this.slides.slideTo(2, 500);
  }
  //Metodo para Validación de campos al usuario
  async validarCampos() {
    //Verificar si todos los campos estan diligenciados
    if(this.usuario.nombre !=='' && this.usuario.correo !=='' && this.usuario.telefono !=='' && this.usuario.password !==''){     
      //Verificar si las contraseñas considen
      if(this.usuario.password == this.passwordConfirm){
        //Metodo para registrar el usuario
        this.registrarUsuario();
      }if(this.usuario.password !== this.passwordConfirm){
          //Metodo de alerta para el usuario "Las contraseñas son diferentes"
          this.showAlert(this.caso=1);
        }
    }else{
      //Metodo de alerta para el usuario "no llenaste algun capo"
      this.showAlert(this.caso=2);    
      }  
  }

  darurl(image){
    this.usuario.url = image.filepath;

  }

  // Metodo para registrar el usuario
  async registrarUsuario(){
   //alert con finalidad de mostrar que se esta cargando
    const loading = await this.loadingController.create({
      message: 'Cargando....'
    });
    await loading.present();
    //Verificar si el usuario existe y registrar en la autentificación de firebase
    this.authService.onRegister(this.user).then((user) => {
      //Asignar id del usuario 
      this.usuario.id = this.authService.getUser();
      //Asignar nuser del usuario congiendo las primeras 4 letras del nombre y aumentandole numeros al azar del 0 al 999
      this.usuario.nuser = this.usuario.nombre.substring(0,4)+Math.round(Math.random()*1000);
      //registrar en la base de firebase
      this.todoService.addUsuario(this.usuario);
      //iniciar en la pagina inicial
      this.nav.navigateForward('/');
      //Cargar alert de cargando...
      loading.dismiss();
    })
    /******************************************************************************
    Muestra que el correo ya esta registrado o la contraseña esta mal escrita 
    o no se escribio el correo correctamente
    *******************************************************************************/
    .catch(err=>{
      //Metodo de alerta para el usuario
      this.showAlert(this.caso=3);
      //Cerrar alert de cargando...
      loading.dismiss();
    })
  }

  //Metodo de alerta al presentarse un error con caso; Identificando el tipo de error
  async showAlert(caso) {
    if(caso==1){
     const prompt = await this.alertCtrl.create({
      header: 'ooops',
      message: 'Las contraseñas no considen',
      buttons: ['OK']
    });
    await prompt.present();

    }if(caso==2){
    const prompt = await this.alertCtrl.create({
      header: 'ooops',
      message: 'Te falta ingresar algun campo',
      buttons: ['OK']
    });
    await prompt.present();

    }if(caso==3){
    const prompt = await this.alertCtrl.create({
      header: 'ooops',
      message: 'el correo ya esta registrado o la contraseña tiene menos de 6 caracteres',
      buttons: ['OK']
    });
    await prompt.present();
    }
  }
}
