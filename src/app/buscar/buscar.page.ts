import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {
    // Variable para guardar la lista de barberos cargados de la base de datos
    usuarios:any [];
    // Variable para guardar y buscar el barbero
    usuario: any = {nuser: null};
    //Variable creada para ocultar mi propio usuario al bucar
    username:string;

  constructor(public fireBaseDB: FirebaseService, private authService: AuthService) { 
    //Obtener id del usuario
    this.username=this.authService.getUser();
  }

  ngOnInit() {
  }
  //Metodo para buscar barbero
  public buscarBarbero(){
    //Si el campo de usuario esta vacio igualar nuser a null para traer todos los usuarios
    if(this.usuario.nuser == ''){
      this.usuario.nuser =null;
      this.fireBaseDB.getUsuarios(this.usuario.nuser).valueChanges().subscribe( usuarios => {
      this.usuarios = usuarios;
      });
    //Si el campo de usuario tiene diligenciado el campo nuser buscar usuario especifico
    }else{
    this.fireBaseDB.getUsuarios(this.usuario.nuser).valueChanges().subscribe( usuarios => {
      this.usuarios = usuarios;
      });
    }
  }
}
