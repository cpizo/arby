import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CasaPage } from './casa.page';

describe('CasaPage', () => {
  let component: CasaPage;
  let fixture: ComponentFixture<CasaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CasaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
