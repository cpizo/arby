import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CasaPageRoutingModule } from './casa-routing.module';

import { CasaPage } from './casa.page';
import { FileSizeFormatPipe5 } from '../services/file-size-format.pipe5';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CasaPageRoutingModule
  ],
  declarations: [CasaPage, FileSizeFormatPipe5]
})
export class CasaPageModule {}
