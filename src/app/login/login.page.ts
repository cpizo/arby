import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../models/usuario.interface';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  //Variable comtiene el user.correo y user.password del usuario
  user: User = new User();

  constructor(private authSvc: AuthService, private router: Router, private alertCtrl: AlertController ) { }

  ngOnInit() {
  }

  //Metodo para Loguear usuario
  async onLogin(){
    //Verifica si el usuario existe
    const user = await this.authSvc.onLogin(this.user)
    //control de errores
    .catch(err=>{
      //Metodo de alerta para el usuario
      this.showAlert();
    });
    if (user) {
      //Redirecciona a la pagina de inicio
      this.router.navigateByUrl('/');
    }
  }

  //Metodo de alert para recuperar la contraseña del usuario
  async olvidarPassword(){
    //Alert1
    const prompt = await this.alertCtrl.create({
       header: 'Olvidaste tu contraseña',
       message: "Por favor introduce tu correo para ayudarte",
       inputs: [
         {
           name: 'correo',
           placeholder: 'Correo'
         },
       ],
       buttons: [
         {
          text: 'Cancelar',
          handler: data => {
             console.log('Cancel clicked');
           }
         },
         //Respuesta a la opción aceptar de alert1
         {
           text: 'Enviar',
           handler: async (data) => {
             //Enviar al metodo restablecer contraseña capturando el correo y enviandolo 
            this.resetPassword(data.correo);
             const alert = await this.alertCtrl.create({
               header: 'Listo',
               subHeader: 'Te enviamos la ayuda a tu correo',
               buttons: ['Aceptar']
             });
             await alert.present();
          }
         }
       ]
    });
     await prompt.present();
  }
   
  //Metodo para recuperar contraseña enviando el email
  resetPassword(email){
    this.authSvc.resetPassword(email);
  }
   
  //Metodo de alerta al presentarse un error
  async showAlert() {
    const prompt = await this.alertCtrl.create({
      header: 'Ooops',
      message: 'La contraseña o el correo estan erroneos',
      buttons: ['OK']
     });
    await prompt.present();
  }
}
