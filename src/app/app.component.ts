import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar, private authService: AuthService, private nav: NavController) {
    //Inicia con el metodo initializeApp
    this.initializeApp();
  }
  
  //Metodo para saber en que pantalla inicializar la app
  initializeApp() {
    //Metodo para saber si la sessión esta iniciada o no
    this.platform.ready().then(() => {
      this.authService.Session.subscribe(session=>{
        //Si la session es tru inicia en la primera pagina del tabs
        if(session){
          this.nav.navigateForward('/');
        }
          //Si la sessión no esta iniciada inicia la app en el login
          else{
            this.nav.navigateForward('/login');
          }
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
