import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImagenPageRoutingModule } from './imagen-routing.module';

import { ImagenPage } from './imagen.page';
import { FileSizeFormatPipe4 } from '../services/file-size-format.pipe4';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImagenPageRoutingModule
  ],
  declarations: [ImagenPage, FileSizeFormatPipe4]
})
export class ImagenPageModule {}
