import { Component, OnInit } from '@angular/core';

import { MyData } from '../models/usuario.interface';
import { Observable } from 'rxjs';
import { FirebaseService } from '../services/firebase.service';
import { Router} from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize, tap } from 'rxjs/operators';


@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.page.html',
  styleUrls: ['./imagen.page.scss'],
})
export class ImagenPage implements OnInit {
  //Url de usuario para traer la imagen de perfil
  usuario: any = {url: null, imagen:null, link:null, fecha:null};
  // Cargar Tarea
  task: AngularFireUploadTask;
  // Progreso en porcentaje
  percentage: Observable<number>;
  // Instantánea de subir archivo
  snapshot: Observable<any>;
  // URL del archivo cargado
  UploadedFileURL: Observable<string>;
  // Lista imagen subida
  images: Observable<MyData[]>;
  //Detalles de archivo  
  fileSize:number;
  //Variables para esconder elementos en el html
  isUploading:boolean;
  isUploaded:boolean;



  constructor(private authService: AuthService, private router: Router,  private firebaseService: FirebaseService, private loadingController: LoadingController, private storage: AngularFireStorage) {
    /* Variables para comenzar con el elemento mostrando 
    foto de perfil actual y boton de cambiar foto */
    this.isUploading = false;
    this.isUploaded = true;
    

/*     const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    this.darkMode = prefersDark.matches;
    console.log(prefersDark);
    document.body.classList.toggle('dark'); */
   }
  ngOnInit() {
        this.firebaseService.getEvento()
        .valueChanges().subscribe(usuario => {
          if(usuario){
          //Carga la variable usuario con los datos traidos de la base de datos
          this.usuario = usuario
            }
            else{
              console.log('Evento no encontrado')
            }
           });
        
  }

uploadFile(event: FileList) { 
  // El objeto del archivo
  const file = event.item(0)
 
  // Validación para lo que se suba sea una imagen
  if (file.type.split('/')[0] !== 'image') {
   console.error('tipo de archivo no es soportado :( ')
    return;
  }
  //Poder ver el elemento de carga en el html
  this.isUploading = true;
  this.isUploaded = false;
 
/*   Ubicación de donde se va a guardar la imagen, van a ser guardada 
  la carpeta con el nombre del id del usuario y el nombre la hora actual */
  const path = `${this.authService.getUser()}/${new Date().getTime()}`;
 
  // Metadato opcional de la imagen
  const customMetadata = { app: 'Imagen subida desde arby app' };
 
  //Variable de referencia de las imagenes subidas
  const fileRef = this.storage.ref(path);
 
  // Subir imagen al almacenamiento de firebase
  this.task = this.storage.upload(path, file, { customMetadata });
 
  // Obtener progreso en porcentaje 
  this.percentage = this.task.percentageChanges();

  this.snapshot = this.task.snapshotChanges().pipe(      
      finalize(() => {
        // Obtener la ruta de almacenamiento del archivo cargado
        this.UploadedFileURL = fileRef.getDownloadURL();
        //Suscribirse para obtener los elementos de la imagen guardada en el almacenamiento
        this.UploadedFileURL.subscribe(resp=>{
          this.actualizarUrlUsuario({name: file.name, filepath: resp, size: this.fileSize, id:Date.now()});
          //Ocultar sesión de carga y mostrar el modulo de foto y botin de cambio de foto
          this.isUploading = false;
          this.isUploaded = true;
        },error=>{
          console.error(error);
        })
      }),
      tap(snap => {
        //Obtener tamaño del archivo
          this.fileSize = snap.totalBytes;
      })
    )
  }
 
  actualizarUrlUsuario(image: MyData) {
    //Variable para guardar el url de la imagen
    this.usuario.url = image.filepath;
  }

  public editarUsuario(){
    // Metodo para editar los datos de la base de datos dependiente del tipo de usuario
    this.usuario.fecha = new Date().getTime();
    this.firebaseService.updateEvento(this.usuario);
    // Metodo para volver a la pagina anterior
    this.router.navigateByUrl('/');
  }  

}